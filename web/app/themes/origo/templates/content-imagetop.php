<section id="image-top">
  <div class="container">
    <div class="row row-eq-height">
      <div class="col-md-5 col-lg-4">
        <h1><?php the_title(); ?></h1>
        <p class="intro"><?php the_field('text'); ?></p>
      </div>
      <div class="col-md-7 col-lg-8 image-min-height">
        <?php $image = get_field('image'); ?>
        <?php if( $image ): ?>
          <div class="image-container">
             <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
