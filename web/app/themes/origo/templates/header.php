<?php
  $topStyle = '';
  $logoStyle = '';
  if (is_front_page()) {
    // $topStyle = ' style="height: 125px !important"';
    // $logoStyle = ' style="margin-top: 79px !important"';
  }
?>
<header class="banner navbar navbar-fixed-top" role="banner"<?php echo $topStyle; ?>>
  <?php
    if (is_front_page()) {
      get_template_part('templates/content', 'hometop');
    }
  ?>
  <div class="container">
    <span id="menu-title" class="hidden-md hidden-lg">
      <span class="text-divider"></span>
      <a class="text-menu" data-toggle="offcanvas" data-target="#primary-menu">
        <div type="button" class="menu-button" data-toggle="offcanvas" data-target="#primary-menu">
          <img src="<?php bloginfo('template_url'); ?>/assets/images/menu_icon.svg" alt="menu-icon">
          <span class="text-title hidden-xs hidden-sm">Meny</span>
        </div>
      </a>
      <?php
        $page ='';
        $page = $wp_query->queried_object->post_title;
        if ($page == 'Startsida') {
          $page = 'Hem';
        }
      ?>
      <?php if($page!=''): ?>
        <span class="text-title page-name hidden-xs hidden-sm">&nbsp;<?php echo $page; ?></span>
      <?php endif; ?>
    </span>
    <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/origo_logo.svg" alt="Origo-logo"<?php echo $logoStyle; ?>></a>

    <div class="desktop-menu hidden-xs hidden-sm">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navmenu-nav','walker' => new Walker_Nav_Menu));
      endif;
      ?>
    </div>

    <div class="right-header-icons">
      <a class="right-header-icon">
        <img src="<?php bloginfo('template_url'); ?>/assets/images/mail_icon.svg" alt="mail-icon" class="ask-us">
      </a>
      <a class="right-header-icon last-top">
        <img src="<?php bloginfo('template_url'); ?>/assets/images/phone_icon.svg" alt="mail-icon" class="call-us">
      </a>
      <a class="right-header-icon hanging" href="http://www.google.se">
        <span>Lämna sidan</span>
        <img src="<?php bloginfo('template_url'); ?>/assets/images/leave_icon.svg" alt="mail-icon">
      </a>
    </div>
  </div>
</header>

<div id="primary-menu" class="navmenu navmenu-default navmenu-fixed-left offcanvas">
  <a type="button" class="close-button" data-toggle="offcanvas" data-target="#primary-menu"><img src="<?php bloginfo('template_url'); ?>/assets/images/menu_close_icon.svg" alt="close-icon"></a>
  <?php
  if (has_nav_menu('primary_navigation')) :
    wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navmenu-nav','walker' => new Walker_Nav_Menu));
  endif;
  ?>
</div>

<div id="black-overlay"></div>
