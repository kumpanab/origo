<?php if (get_field('splash_text') != ''): ?>
  <?php
    $blockClass = '';
    $blockClass = 'class="'.get_field('splash_color').'"';
   ?>
  <section id="splash" <?php echo $blockClass ?>>
    <div class="container">
      <div class="row">
        <div class="col-sm-push-0 col-sm-12 col-md-push-1 col-md-10 col-lg-push-2 col-lg-8">
          <img class="heart" src="<?php bloginfo('template_url'); ?>/assets/images/hjarta.png" alt="hjärta" />
          <?php the_field('splash_text'); ?>
        </div>
      </div>
    </div>
  </section>
<?php endif ?>
