<section id="home-top">
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-md-push-7 col-lg-4 col-lg-push-8">
        <?php the_field('text-block'); ?>
      </div>
      <div class="col-md-7 col-md-pull-5 col-lg-8 col-lg-pull-4">
        <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
          <div class="carousel-inner">
            <?php $i = 0;if( have_rows('images') ): ?>
              <?php while( have_rows('images') ): the_row();
                $image = get_sub_field('image');
                $linkText = get_sub_field('link_text');
                $link = get_sub_field('link');
              ?>
                <?php
                  if($i == 0){
                    $activeClass = 'active ';
                  }else{
                    $activeClass = '';
                  }
                ?>
                <div class="<?php echo $activeClass ?>item">
                  <?php if( $image ): ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                  <?php endif; ?>
                  <?php if( $link ): ?>
                    <a href="<?php echo $link; ?>" class="button"><button><span><?php echo $linkText; ?></span></button></a>
                  <?php endif; ?>
                </div>
              <?php $i++;endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>

  </div>

</section>

<?php get_template_part('templates/content', 'linkbar'); ?>
