<section id="no-image-top">
  <div class="container">
    <div class="row">
      <div class="col-sm-push-0 col-sm-12 col-md-push-1 col-md-10 col-lg-push-2 col-lg-8">
        <h1><?php the_title(); ?></h1>
        <div class="title-underline"></div>
        <?php if (get_field('intro') != ''): ?>
          <p class="intro"><?php the_field('intro'); ?></p>
        <?php endif ?>
        <?php if (get_field('text') != ''): ?>
          <p><?php the_field('text'); ?></p>
        <?php endif ?>
      </div>
    </div>
  </div>
</section>
