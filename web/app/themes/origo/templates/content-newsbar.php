<section id="news-bar">
  <div class="container">
    <div class="row">
      <?php
        function excerpt($limit) {

        }
        $news = array();
        $args = array( 'posts_per_page' => '3','post_type' => 'post' );
        $recent_posts = new WP_Query( $args );
        if( $recent_posts->have_posts() ) :
          while( $recent_posts->have_posts() ) :
            $recent_posts->the_post();
            $excerpt = explode(' ', get_the_excerpt(), 10);
            if (count($excerpt)>=10) {
              array_pop($excerpt);
              $excerpt = implode(" ",$excerpt).'...';
            } else {
              $excerpt = implode(" ",$excerpt);
            }
            $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
            array_push($news, '<p><a href="' . get_permalink() . '">'.get_the_title().'</a></p><p>'.$excerpt.'</p>');
          endwhile;
        endif;
      ?>
      <div class="col-sm-12 col-md-4">
        <?php echo $news[0]; ?>
      </div>
      <div class="col-sm-12 col-md-4">
        <?php echo $news[1]; ?>
      </div>
      <div class="col-sm-12 col-md-4">
        <?php echo $news[2]; ?>
      </div>
    </div>
  </div>
</section>
