<?php while (have_posts()) : the_post(); ?>
  <section id="text-blocks">
    <div class="text-block">
      <div class="container">
        <div class="row">
          <div class="col-sm-push-0 col-sm-12 col-md-push-1 col-md-10 col-lg-push-2 col-lg-8">
            <article <?php post_class(); ?>>
              <header>
                <h1 class="entry-title"><?php the_title(); ?></h1>
                <div class="title-underline"></div>
                <?php get_template_part('templates/entry-meta'); ?>
              </header>
              <div class="entry-content">
                <?php the_content(); ?>
              </div>
              <div class="title-underline top-margin"></div>
            </article>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-push-0 col-sm-12 col-md-push-1 col-md-10 col-lg-push-2 col-lg-8">
              <span class="post-nav prev-posts"><?php previous_post_link('%link', 'Äldre') ?></span>
              <span class="post-nav all-posts"><a href="/arbetar-du-med-unga/nyhetsflode">Alla nyheter</a></span>
              <span class="post-nav next-posts"><?php next_post_link('%link', 'Nyare') ?></span>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endwhile; ?>
