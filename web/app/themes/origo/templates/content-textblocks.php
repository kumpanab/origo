<section id="text-blocks">
  <?php if( have_rows('text-blocks') ): ?>
    <?php while( have_rows('text-blocks') ): the_row();
      $blockClass = '';
      $blockClass = get_sub_field('color');
    ?>
      <div class="text-block <?php echo $blockClass ?>">
        <div class="container">
          <div class="row">
            <div class="col-sm-push-0 col-sm-12 col-md-push-1 col-md-10 col-lg-push-2 col-lg-8">
              <?php echo get_sub_field('text') ?>
            </div>
          </div>
        </div>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>
</section>


