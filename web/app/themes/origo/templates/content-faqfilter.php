<section id="faq-filter">
  <div class="container">
    <div class="row">
      <div class="col-sm-push-0 col-sm-12 col-md-push-2 col-md-8">
        <span>Filtrera:</span>
        <input id="faq-search" type="text" placeholder="Skriv ditt sökord här">
      </div>
    </div>
  </div>
</section>
