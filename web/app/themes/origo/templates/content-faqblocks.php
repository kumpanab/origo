<section id="faq-blocks">
  <div class="container">
    <?php if( have_rows('faq-blocks') ): ?>
      <?php $i=0;while( have_rows('faq-blocks') ): the_row(); ?>
        <div class="row">
          <div class="col-sm-push-0 col-sm-12 col-md-push-2 col-md-8">
            <h2 class="faq-title"><?php echo get_sub_field('title') ?></h2>
            <?php if( have_rows('questions') ): ?>
              <?php $j=0;while( have_rows('questions') ): the_row(); ?>
                <h3 onclick="" data-toggle="collapse" data-target="#faq-answer-<?php echo $i;echo $j; ?>"><span id="<?php echo ($i + 1).'-'.($j + 1) ?>"></span><?php echo get_sub_field('question') ?></h3>
                <p id="faq-answer-<?php echo $i;echo $j; ?>" class="collapse"><?php echo get_sub_field('answer') ?></p>
              <?php $j++;endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      <?php $i++;endwhile; ?>
    <?php endif; ?>
  </div>
</section>

