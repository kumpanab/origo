<footer>
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-4">
        <button class="call-us"><span>Ring oss</span></button>
      </div>
      <div class="col-sm-12 col-md-4">
        <button class="ask-us"><span>Fråga origo</span></button>
      </div>
      <div class="col-sm-12 col-md-4">
        <a href="/vanliga-fragor" class="button"><button><span>Vanliga frågor</span></button></a>
      </div>
    </div>
    <div class="row icons-row">
      <div class="col-xs-12">
        <img class="social-icon hbtq" src="<?php bloginfo('template_url'); ?>/assets/images/hbtq_diplom.svg" alt="hbtq-logo"/>
      </div>
    </div>
    <div class="row icons-row">
      <div class="col-xs-12">
        <a href="<?php the_field('facebook_link', 'option'); ?>"><img  class="social-icon middle-icon" src="<?php bloginfo('template_url'); ?>/assets/images/icon_footer_facebook.svg" alt="facebook-logo"></a>
        <a href="<?php the_field('instagram_link', 'option'); ?>"><img  class="social-icon" src="<?php bloginfo('template_url'); ?>/assets/images/icon_footer_instagram.svg" alt="instagram-logo"></a>
      </div>
    </div>
  </div>
</footer>

<div id="dark-popup">
  <div class="content">
    <div class="inner-content">
      <img id="dark-popup-close" src="<?php bloginfo('template_url'); ?>/assets/images/menu_close_icon.svg" alt="stäng_icon">

      <div class="call-us-content">
        <?php the_field('call-us', 'option'); ?>
      </div>

      <div class="ask-us-content">
        <?php the_field('ask-us', 'option'); ?>
    </div>
  </div>
</div>
