<section id="link-blocks">
  <div class="container">
    <?php if( have_rows('link-blocks') ): ?>
      <?php while( have_rows('link-blocks') ): the_row(); ?>
        <div class="row">
          <div class="col-sm-push-0 col-sm-12 col-md-push-1 col-md-10 col-lg-push-2 col-lg-8">
            <?php if ( get_sub_field('text') == '' ) : ?>
              <hr>
              <h3><?php echo get_sub_field('title') ?></h3>
            <?php else : ?>
              <h2><img class="link-arrow" src="<?php bloginfo('template_url'); ?>/assets/images/link_arrow.svg" alt="Länkpil"><?php echo get_sub_field('title') ?></h2>
              <p>
                <?php echo get_sub_field('text') ?> <a href="<?php echo get_sub_field('link') ?>">Läs mer</a>
              </p>
            <?php endif ?>

          </div>
        </div>
      <?php endwhile; ?>
    <?php endif; ?>
  </div>
</section>
