<section id="home-private-top">
  <div class="container">
    <div class="row">
      <p class="large">Dölj ditt besök!</p>
      <p>Är du orolig över att ditt besök sparas i webbläsaren? Här berättar vi hur du kan dölja ditt besök och "<a href="/dolj-ditt-besok">surfa privat</a>".</p>
      <img id="home-private-top-close" src="<?php bloginfo('template_url'); ?>/assets/images/leave_close_icon.svg" alt="stäng_icon">
    </div>
  </div>
</section>
