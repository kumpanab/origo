<section id="link-bar">
  <div class="container">
    <div class="row">
      <a href="/ar-du-utsatt/karlek/" class="col-xs-6 col-sm-4 col-md-2">
        <img class="link-bar-icon" src="<?php bloginfo('template_url'); ?>/assets/images/icon_karlek.svg" alt="Kärlekicon">
        <span>Kärlek</span>
      </a>
      <a href="/ar-du-utsatt/vanner/" class="col-xs-6 col-sm-4 col-md-2">
        <img class="link-bar-icon" src="<?php bloginfo('template_url'); ?>/assets/images/icon_vanner.svg" alt="Vännericon">
        <span>Vänner</span>
      </a>
      <a href="/ar-du-utsatt/familj/" class="col-xs-6 col-sm-4 col-md-2">
        <img class="link-bar-icon" src="<?php bloginfo('template_url'); ?>/assets/images/icon_familj.svg" alt="Familjicon">
        <span>Familj</span>
      </a>
      <a href="/ar-du-utsatt/kropp-kon/" class="col-xs-6 col-sm-4 col-md-2">
        <img class="link-bar-icon" src="<?php bloginfo('template_url'); ?>/assets/images/icon_kropp_kon.svg" alt="Kropp & kön-icon">
        <span>Kropp & kön</span>
      </a>
      <a href="/ar-du-utsatt/giftermal/" class="col-xs-6 col-sm-4 col-md-2">
        <img class="link-bar-icon" src="<?php bloginfo('template_url'); ?>/assets/images/icon_giftermal.svg" alt="Giftermålicon">
        <span>Giftermål</span>
      </a>
      <a href="/ar-du-utsatt/dina-rattigheter/" class="col-xs-6 col-sm-4 col-md-2">
        <img class="link-bar-icon" src="<?php bloginfo('template_url'); ?>/assets/images/icon_rattigheter.svg" alt="Dina rättigheter-icon">
        <span>Dina rättigheter</span>
      </a>
    </div>
  </div>
</section>
