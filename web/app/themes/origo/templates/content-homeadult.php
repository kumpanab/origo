<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/scripts/YouTubePopUp.jquery.js"></script>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/styles/components/YouTubePopUp.css">
<section id="adult-home-top">
  <div class="container top-container">
    <div class="row row-eq-height">
      <div class="col-md-5 col-lg-4">
        <div class="text-container">
          <h1><?php the_title() ?></h1>
          <p class="intro">
            <?php the_field('text-intro'); ?>
          </p>
          <p>
            <?php the_field('text-block'); ?>
          </p>
        </div>
      </div>
      <div class="col-md-7 col-lg-8 full-height-gray">
        <div class="carousel-container">
          <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
              <?php $i = 0;if( have_rows('block_texts') ): ?>
                <?php while( have_rows('block_texts') ): the_row();
                  $blockText = get_sub_field('block_text');
                  $image = get_sub_field('image');
                  $title = get_sub_field('title');
                ?>
                  <?php
                    if($i == 0){
                      $activeClass = 'active ';
                    }else{
                      $activeClass = '';
                    }
                  ?>
                  <div class="<?php echo $activeClass ?>item">
                    <?php if (get_sub_field('video')): ?>
                      <a href="<?php echo get_sub_field('link'); ?>" class="video-link"><img src="<?php echo $image['url'];?>" alt=""></a>
                    <?php else: ?>
                      <a href="<?php echo get_sub_field('link'); ?>"><img src="<?php echo $image['url'];?>" alt=""></a>
                    <?php endif ?>

                    <div class="text-container-outer">
                      <div class="text-container-inner">
                        <h2><?php echo $title; ?></h2>
                        <p><?php echo $blockText; ?></p>
                      </div>
                    </div>
                  </div>
                <?php $i++;endwhile; ?>
              <?php endif; ?>
<!--               <ol class="carousel-indicators">
                <?php for ($j=0; $j < $i; $j++) {
                  if($j == 0){
                    $activeClass = 'class="active"';
                  }else{
                    $activeClass = '';
                  }
                  echo '<li data-target="#carousel" data-slide-to="'.$j.'" '.$activeClass.'></li>';
                } ?>
              </ol> -->
            </div>
            <div class="chevron-container">
              <a class="left" href="#carousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
              </a>
              <a class="right" href="#carousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

<section id="news-bar">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-8">
        <h2><a href="/arbetar-du-med-unga/nyhetsflode">Senaste nyheterna</a></h2>
      </div>
      <div class="col-sm-12 col-md-4">

      </div>
    </div>
    <div class="row">
      <?php
        function excerpt($limit) {

        }
        $news = array();
        $args = array( 'posts_per_page' => '3','post_type' => 'post' );
        $recent_posts = new WP_Query( $args );
        if( $recent_posts->have_posts() ) :
          while( $recent_posts->have_posts() ) :
            $recent_posts->the_post();
            $excerpt = explode(' ', get_the_excerpt(), 20);
            if (count($excerpt)>=20) {
              array_pop($excerpt);
              $excerpt = implode(" ",$excerpt).'...';
            } else {
              $excerpt = implode(" ",$excerpt);
            }
            $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
            array_push($news, '<p class="title"><a href="' . get_permalink() . '">'.get_the_title().'</a></strong></p><p>'.$excerpt.' <a href="' . get_permalink() . '">Läs mer</a></p>');
          endwhile;
        endif;
        wp_reset_query();
      ?>
      <style type="text/css">
        #news-bar p.title a{
          font-weight: 800;
        }
      </style>
      <div class="col-sm-12 col-md-4 first-news">
        <?php echo $news[0]; ?>
      </div>
      <div class="col-sm-12 col-md-4">
        <?php echo $news[1]; ?>
      </div>
        <?php echo $news[2]; ?>
    </div>
  </div>
</section>

<?php get_template_part('templates/content', 'textblocks'); ?>
