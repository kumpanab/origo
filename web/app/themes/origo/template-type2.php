<?php
/**
 * Template Name: Type2 Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'type2'); ?>
<?php endwhile; ?>
