/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        if($('.image-min-height').length) {
          $('.image-min-height').css('min-height', $('.image-min-height .image-container img').height() + 'px');
        }
        $('.image-min-height .image-container img').load(function() {
          if($('.image-min-height').length) {
            $('.image-min-height').css('min-height', $('.image-min-height .image-container img').height() + 'px');
          }
        });

        // primary side menu show and hide functions
        $('#primary-menu').on('show.bs.offcanvas', function(e) {
          $('#black-overlay').fadeIn(100);
        });
        $('#primary-menu').on('hide.bs.offcanvas', function(e) {
          $('#black-overlay').fadeOut(100);
        });

        $('.call-us').click(function() {
          $('#dark-popup').fadeIn(100);
          $('.call-us-content').fadeIn(100);
        });

        $('.ask-us').click(function() {
          $('#dark-popup').fadeIn(100);
          $('.ask-us-content').fadeIn(100);
        });

        $('#dark-popup, #dark-popup .content').click(function(e) {
          if(e.target != this) return;
          $('#dark-popup').fadeOut(100);
          $('.call-us-content').fadeOut(100);
          $('.ask-us-content').fadeOut(100);
        });

        $('#dark-popup-close').click(function() {
          $('#dark-popup').fadeOut(100);
          $('.call-us-content').fadeOut(100);
          $('.ask-us-content').fadeOut(100);
        });

        $('.desktop-menu .menu-item-has-children').hover(function() {
          $('.sub-menu', this).stop().slideDown(150, 'linear');
        });
        $('.desktop-menu .menu-item-has-children').mouseleave(function(e) {
          if(!$(e.toElement).hasClass('sub-menu')) {
            $('.sub-menu', this).stop().slideUp(150, 'linear');
          }
        });


      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {

        //var bodyTopMargin = 55 + $('#home-private-top').height();
        //$('body').css('margin-top', bodyTopMargin + 'px');

        $('.carousel').carousel({
         interval : 5000
        });

        $('#home-private-top-close').click(function() {
          $('#home-private-top').remove();
          //$('body').css('margin-top', '55px');
          //$('header').css('height', '55px');
          //$('.navbar-brand img').css('margin-top', '9px');
        });

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    'arbetar_du_med_unga': {
      init: function() {

        // if ((window.innerWidth > 991)){
        //   if ($('.text-container').css('top') != '0px') {
        //     $('.full-height').height($('.top-container').height());
        //   }
        //   $(window).resize(function(){
        //     if ($('.text-container').css('top') != '0px') {
        //       $('.full-height').height($('.top-container').height());
        //     }else{
        //       $('.full-height').css('height','auto');
        //     }
        //   });
        // }
        $('.video-link').YouTubePopUp();

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    'vanliga_fragor': {
      init: function() {

        if (window.location.hash) {
          $(window.location.hash).parent().next().collapse("toggle");
        }

        $('#faq-search').keyup(function() {
          $('#faq-blocks .row').each(function(index) {
            var $this = $(this);
            if ($('h2', $this).text().toLowerCase().indexOf($('#faq-search').val().toLowerCase()) === -1) {
              $($this).hide();
              $('h3', $this).each(function(index) {
                if ( ($(this).text().toLowerCase().indexOf($('#faq-search').val().toLowerCase()) === -1 ) && ($(this).next().text().toLowerCase().indexOf($('#faq-search').val().toLowerCase()) === -1 )) {
                  $(this).hide();
                  $(this).next().fadeOut();
                }else{
                  $(this).show();
                  $($this).show();
                }
              });
            }else{
              $($this).show();
              $('h3', $this).each(function(index) {
                $(this).show();
              });
            }

          });
        });
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
