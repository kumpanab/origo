<section id="no-image-top">
  <div class="container">
    <div class="row">
      <div class="col-sm-push-0 col-sm-12 col-md-push-1 col-md-10 col-lg-push-2 col-lg-8">
        <h1><?php the_field('title', get_option('page_for_posts')); ?></h1>
        <div class="title-underline"></div>
        <?php if (get_field('intro', get_option('page_for_posts')) != ''): ?>
          <p class="intro"><?php the_field('intro', get_option('page_for_posts')); ?></p>
        <?php endif ?>
        <?php if (get_field('text', get_option('page_for_posts')) != ''): ?>
          <p><?php the_field('text', get_option('page_for_posts')); ?></p>
        <?php endif ?>
      </div>
    </div>
  </div>
</section>
<section id="text-blocks">
  <?php $i = 0; while (have_posts()) : the_post(); ?>
    <div class="text-block <?php if($i%2 == 0){echo 'gray';} ?>">
      <div class="container">
        <div class="row">
          <div class="col-sm-push-0 col-sm-12 col-md-push-1 col-md-10 col-lg-push-2 col-lg-8">
            <article <?php post_class(); ?>>
              <header>
                <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <?php get_template_part('templates/entry-meta'); ?>
              </header>
              <div class="entry-summary">
                <p>
                  <?php echo get_the_excerpt(); ?><a href="<?php the_permalink(); ?>"> Läs mer</a>
                </p>
              </div>
            </article>
          </div>
        </div>
      </div>
    </div>
  <?php $i++; endwhile; ?>
  <section class="news-navigation <?php if($i%2 == 0){echo 'gray';} ?>">
    <div class="container">
      <div class="row">
        <div class="col-sm-push-0 col-sm-12 col-md-push-1 col-md-10 col-lg-push-2 col-lg-8">
          <div class="title-underline"></div>
          <?php the_posts_navigation(); ?>
        </div>
      </div>
    </div>
  </section>
</section>



